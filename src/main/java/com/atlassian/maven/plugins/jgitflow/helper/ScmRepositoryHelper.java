package com.atlassian.maven.plugins.jgitflow.helper;

import org.apache.maven.scm.repository.ScmRepository;

/**
 * @since version
 */
public interface ScmRepositoryHelper
{
    ScmRepository getRepository();
}
